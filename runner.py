#import ipdb; ipdb.set_trace()

import csv
import codecs
import tempfile
import ftplib
import zipfile
import logging
import sys
import json
import os
import io
from datetime import datetime, timedelta
from google.cloud import bigquery

from raven import Client

from handlers import StorageHandler, FTPHandler, BigQueryHandler, ComputeInstanceHandler, InstanceCreationException
from config import Config

yesterday = datetime.now() - timedelta(days=1)
yesterday_txt = yesterday.strftime("%Y-%m-%d")
#file_names = []
file_names_short = ['transactions', 'visits_dates', 'all_emea']
#for i in file_names_short:
#    tmp = ''.join(['export_attribution_altima', '_', i, '_', yesterday_txt])
#    file_names.append(tmp)
#print(file_names)

server = 'ftp3.omniture.com'
user = 'ubidata'
password = 'xjV9.qLy'
source = '/'
destination = '/home/alex/work/ubisoft_altima_data_transfer/data'
interval = 0.05

dataset_id = 'Altima_adobe_attribution'

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

sentry = Client('https://d98aa062665b49ddbc4c934a7a85168e:9a4412a987604d37a2b1dc59c5b324f6@sentry.io/300386')
BASE_DIR = os.path.dirname(__file__)


class DataTransferer(object):
    def __init__(self):
        self.config = Config(os.path.join(BASE_DIR, 'runner.ini'))
        self.ftp_fperfix = 'FTP' #I'm not sure if it's suitable here

        self.file_names = file_names_short
        #self.file_names = ['altima_example_attribution.zip']  # test value

    def run(self, to_bq=True):
        for fname in self.file_names:
            with tempfile.TemporaryFile() as ftp_file:
                ftp = ftplib.FTP(server)
                ftp.login(user, password)
                fpath = os.path.join(self.ftp_fperfix, fname)
                logger.info('Starting download from ftp {}'.format(fpath))
                fname_zip = ''.join(['export_attribution_altima', '_', fname, '_', yesterday_txt, '.zip'])
                #fname_zip = 'altima_example_attribution.zip' # for test only
                ftp.retrbinary("RETR " + fname_zip, ftp_file.write)
                logger.info('Downloaded! from ftp {}'.format(fpath))
                ftp_file.seek(0)
                zip_ref = zipfile.ZipFile(ftp_file, 'r')
                zip_ref.extractall()
                zip_ref.close()
#
                if to_bq:
                    table_id = fname
                    fname_csv = ''.join(['export_attribution_altima', '_', fname, '_', yesterday_txt, '.csv'])
                    #table_id = 'tmpdlt' # for test only
                    #fname_csv = 'altima_example_attribution.csv'  # for test only
                    self.write_to_bq(dataset_id, table_id, fname_csv)

        msg = 'DONE: Successfully transferred {}'.format(self.file_names)
        logger.info(msg)

    def write_to_bq(self, dataset_id, table_id, source_file_name):
        bigquery_client = bigquery.Client()
        dataset_ref = bigquery_client.dataset(dataset_id)  # or dataset name?
        table_ref = dataset_ref.table(table_id)
        #table = bigquery.Table(table_ref)

        with open(source_file_name, 'rb') as source_file:
            job_config = bigquery.LoadJobConfig()
            job_config.source_format = 'CSV'
            job_config.skip_leading_rows = 1
            #job_config.max_bad_records = 100
            #job_config.allow_jagged_rows = True
            #job_config.ignore_unknown_values = True
            #job_config.write_disposition = 'WRITE_APPEND'

            logger.info('Uploading {file} from instance to BigQuery table: {table}'.format(file=fname_csv, table=table_ref))
            #job = bigquery_client.load_table_from_file(source_file, table_ref, job_config=job_config)

            try:
                job = bigquery_client.load_table_from_file(source_file, table_ref, job_config=job_config)
            except Exception as e:
                logger.critical('Cant upload to BQ. file: {file}\nError:\n{e}'.format(file=fpath, e=str(e)))

        job.result()  # Waits for job to complete

        print('Loaded {} rows into {}:{}.'.format(
            job.output_rows, dataset_id, table_id))


if __name__ == '__main__':
    DataTransferer = DataTransferer()
    DataTransferer.run()