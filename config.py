from configparser import ConfigParser
import io


class Config(object):
    def __init__(self, fname: str):
        # TODO: make me lazy
        self.cp = ConfigParser()
        self.cp.read(fname)
        self.set_keys()

    def set_keys(self):
        for section in self.cp.sections():
            for k, v in self.cp.items(section):
                key = '{section}_{key}'.format(section=section, key=k).upper()
                setattr(self, key, v)