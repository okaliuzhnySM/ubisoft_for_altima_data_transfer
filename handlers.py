import datetime
from ftplib import FTP_TLS
import os
import io
import logging
import sys
from typing import List, Iterable
import time

from google.oauth2 import service_account
from google.cloud import bigquery, storage, exceptions
from google.cloud.bigquery import SchemaField
from oauth2client.service_account import ServiceAccountCredentials
import googleapiclient.discovery


BASE_DIR = os.path.dirname(__file__)

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


class InstanceCreationException(Exception):
    pass


class BigQueryHandler(object):
    def __init__(self, key: str, project: str, ds_name: str):
        """
        :param key: filename of private key file to cloud
        :param project: name of the project to use
        :param ds_name: dataset name. To what dataset upload the data.
        """
        self.key = key
        self.project = project
        self.dataset = ds_name

    def upload(self, cs_path: str, table_name: str, schema: List[SchemaField]=None):
        """
        Upload csv data from cloud storage to bigquery, creating table if needed.
        :param cs_path: full path in cloud storage to uploading file
        :param table_name: table name where data should be written
        :param schema: (optional) schema table to be created.
        :return:
        """
        bq_creds = service_account.Credentials.from_service_account_file(os.path.join(BASE_DIR, self.key))
        client = bigquery.Client(project=self.project, credentials=bq_creds)
        dataset = client.dataset(self.dataset)
        if not dataset.exists():
            dataset.create()

        table = dataset.table(name=table_name)
        if not table.exists():
            if schema is None:
                raise Exception('Table doesnt exist. You should provide `schema` to create table')
            else:
                table.schema = schema
                table.create()

        job_name = 'load-from-storage-{table}_{ts}'.format(
            table=table_name,
            ts=str(datetime.datetime.now()).split('.')[0].replace(' ', '_').replace(':', '_'))
        job = client.load_table_from_storage(job_name, table, cs_path)
        job.source_format = 'CSV'
        job.skip_leading_rows = 1
        job.write_disposition = 'WRITE_TRUNCATE'
        logger.info('Job created: {name}'.format(name=job_name))
        job.begin()
        try:
            job.result(3600)  # one hour
        except exceptions.BadRequest:
            job.allow_jagged_rows = True
            job.allow_quoted_newlines = True
            job.begin()
            job.result(3600)
        logger.info('Job returned. State: {state}'.format(state=job.state))

    @staticmethod
    def table_name(candidate: str) -> str:
        """
        Transform `candidate` string to valid table name
        """
        name = candidate.split('/')[-1]
        return name.lower().replace(' ', '_').replace('.csv', '')

    @staticmethod
    def generate_schema(fields: Iterable[str]) -> List[SchemaField]:
        """
        Generate schema for table
        :param fields: list of desired fields name
        """
        return [SchemaField(field.replace('#', ''), 'STRING') for field in fields]


class FTPHandler(object):
    def __init__(self, host: str, login: str, pwd: str):
        """
        :param host: Host address
        :param login: Login to ftp
        :param pwd: password to ftp
        """
        self.ftp = FTP_TLS(host)
        self.ftp.login(login, pwd)
        logger.info('Connected to ftp: {}'.format(host))
        self.ftp.voidcmd('TYPE I')
        self.ftp.prot_p()

    def download(self, fname: str, file: io.FileIO) -> None:
        """
        Download file name `fname` from ftp to `file`
        """
        self.ftp.retrbinary('RETR ' + fname, file.write)


class StorageHandler(object):
    def __init__(self, key: str, bucket_name: str, blob_prefix: str, blob_name: str):
        """
        :param key: filename of private key file to cloud
        :param bucket_name: in what bucket upload file
        :param blob_prefix: `directory` to put blob into
        :param blob_name:  blog name where data will be hold
        """
        self.storage_client = storage.Client.from_service_account_json(key)
        self.bucket_name = bucket_name
        self.blob_name = blob_prefix + blob_name

    def upload(self, file: io.FileIO) -> None:
        try:
            bucket = self.storage_client.get_bucket(self.bucket_name)
        except exceptions.NotFound:
            logger.info('Created bucket: {}'.format(self.bucket_name))
            bucket = self.storage_client.create_bucket(self.bucket_name)

        blob = bucket.blob(self.blob_name, chunk_size=262144)
        blob.upload_from_file(file, content_type='text/csv', rewind=True)

    def gs_path(self) -> str:
        """
        Return fully qualified google storage path, using bucket_name and blob_name from __init__ method.
        """
        return 'gs://{bucket}/{blob}'.format(bucket=self.bucket_name, blob=self.blob_name)


class ComputeInstanceHandler(object):
    def __init__(self, key, name, project, zone, machine='n1-standard-1'):
        self.project = project
        self.machine = machine
        self.zone = zone
        self.name = name
        credentials = ServiceAccountCredentials.from_json_keyfile_name(key)
        self.compute = googleapiclient.discovery.build('compute', 'v1', credentials=credentials)

    def _create(self):
        image_response = self.compute.images().getFromFamily(
            project='ubuntu-os-cloud', family='ubuntu-1604-lts').execute()
        source_disk_image = image_response['selfLink']

        # Configure the machine
        config = {
            'name': self.name,
            'machineType': 'zones/europe-west1-b/machineTypes/{machine}'.format(machine=self.machine),
            'disks': [
                {
                    'boot': True,
                    'autoDelete': True,
                    'initializeParams': {
                        'sourceImage': source_disk_image,
                    }
                }
            ],
            'networkInterfaces': [{
                'network': 'global/networks/default',
                'accessConfigs': [
                    {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
                ]
            }],
            'serviceAccounts': [{
                'email': 'default',
                'scopes': [
                    'https://www.googleapis.com/auth/devstorage.read_write',
                    'https://www.googleapis.com/auth/logging.write'
                ]
            }],
            'metadata': {
                'items': [{
                    'key': 'startup-script',
                    'value': 'echo test'
                }]
            }
        }

        return self.compute.instances().insert(
            project=self.project,
            zone=self.zone,
            body=config).execute()

    def wait_for_operation(self, operation):
        while True:
            result = self.compute.zoneOperations().get(
                project=self.project,
                zone=self.zone,
                operation=operation).execute()

            if result['status'] == 'DONE':
                print("done.")
                if 'error' in result:
                    raise InstanceCreationException(result['error'])
                return result

            time.sleep(1)

    def create(self):
        logger.info('Creating VM: {name} for {project}. Details: {machine}, {zone}'
                    .format(name=self.name, project=self.project, machine=self.machine, zone=self.zone))
        operation = self._create()
        self.wait_for_operation(operation['name'])
        logger.info('Created VM: {name} for {project}. Details: {machine}, {zone}'
                    .format(name=self.name, project=self.project, machine=self.machine, zone=self.zone))

    def list_instances(self):
        return self.compute.instances().list(project=self.project, zone=self.zone).execute()

    def delete(self):
        return self.compute.instances().delete(project=self.project, zone=self.zone, instance=self.name).execute()

    def provision(self):
        pass